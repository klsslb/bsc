
One of the goals of the project was to to keep the user API as close to the
original as possible.
New ideas arose during the implementation which made us go back and revise
the design. It was a back-and-forth process, but you rarely land at the perfect
design the first time around. We intended to reuse as much of the original
implementation as possible, but in the process we ended up having to alter most
of the existing classes. This was mainly due to the architecture changes
described in \prettyref{ch:design}.
This chapter will briefly discuss the addition of new classes and the
alterations that were necessary to the existing classes.

\section{HashCode and Equals}
Java requires classes to implement a \method{hashCode} and \method{equals}
method in order for objects to be comparable and hashable. Some of the existing
classes had their own implementation of these two methods, and some of the
classes we added along the way when needed. Since the practice is common, libraries
for this purpose exist, and we felt it was less prone to programmer mistakes
to use a library for the purpose instead of implementing the methods by hand.
We replaced the current implementations with third party library\cite{apachecommons} methods
from \class{EqualsBuilder} and \class{HashCodeBuilder}.
To simplify the testing of these methods, a library \class{EqualsVerifier}
\cite{equalsverifier} was used to test the library methods. Again, this decision was to reduce the
amount of work we had to do, and potential programmer errors.

\section{Messages}

Since the two message structures defined in \prettyref{sec:messages} are both
types of messages and share most of their fields, we decided to create a
common super class \class{Message}. The class \class{pSpaceMessage} extended
this with the fields required by the message used for space operations, and
\class{ManagementMessage} adds the fields required by the message used for
managing the server.

The classes are simple, and mainly consists of the fields they need. They exist
purely for structural purposes so that the marshaller may marshal messages to
the representation that protocol expects and so that they can be used
internally in the library.

\section{Server}

The existing library had a comprehensive implementation of a server. It was
used in the \class{SpaceRepository} class and included message handlers and
an inbox/outbox construct that used rendezvous synchronization mechanism\cite{concurrentbook}
to synchronize incoming and outgoing messages, which we think is worth keeping.
In our design we needed two quite similar servers, one that handles
\class{ManagementMessage}s and one that handles \class{pSpaceMessage}s.
The difference between the two is only what message class they expect to
receive and the way they handle them. A \class{Server} super class was created,
with subclasses \class{ManagementServer} and \class{Repository}.

The \class{ManagementServer} keeps track of all the \class{Repositories}
and \class{SpaceWrapper}s in existence on the server.
A \class{Space} and a \class{Repository} run in their
own thread. Each \class{Repository} has an additional two threads for
handling incoming and outgoing messages, which it inherits from extending the
\class{Server} class.

\subsection{SpaceWrapper}

A \class{SpaceWrapper} is a wrapper around an underlying implementation of a
\class{Space} while attaching a \class{SpaceProperties} object.
This object contains vital information like the keys used for access control.


\section{Client changes}
The client is the application that is intended to be modified by the end
users.

The client code introduces a couple of new classes used for connecting to the
server. The classes exist mainly to ease the use of the client API, and ensure
that the user experience is similar to that of the current implementation.
The following subsections will summarize their implementation.

\subsection{ServerConnection}
The \class{ServerConnection} is used in clients for creating a gate to the
server, while supplying the client with a set of methods for interacting with
the server. The \class{SpaceRepository} needs a reference to the \class{ServerConnection}
that the user initializes in the beginning of a jSpace application, which it
uses to send messages for space creation.
We chose to let the \class{ServerConnection} class be
static, so that only a single instance can exist.
The alternative solution was to introduce another method parameter to the
constructor of \class{SpaceRepository}, but it would mean that the client API
would change.
The decision limits a jSpace application to only have access to a single
management server.

It is accomplished by marking the constructor methods of the \class{ServerConnection}
class private, and then introduce public \method{getInstance()}methods which
will either instantiate a new object or return the existing one depending on
the current state of the program.


\subsection{SpaceRepository}
The class name \class{SpaceRepository} was reused from the old design, but the
behavior of the class has changed drastically. It has a method for creating
new spaces, and a \class{Gate} which connects it to the \class{Repository}
residing in the server application.

\subsection{NamedSpace}
A \class{NamedSpace} exists merely to stay true to the original client
API. It serves as the user interface to the former space implementations, and
method calls are propagated to the repository to which it belongs where they
are prepared as messages and sent to the remote repository server.


\section{Gates}
Gates are an abstraction of network connections. In jSpace, the only option
for a gate is a TCP socket connection, which can run in either \class{CONN}
or \class{KEEP} mode.

For the application developer the life of a gate is initialized by executing
\class{addGate(uri)} on a repository. \class{addGate()} uses a
\class{GateFactory}, gets its \class{GateBuilder} and calls
\class{createServerGate(uri)} on the \class{GateBuilder}.

The \class{createServerGate(uri)} method parses the URI to get the host, port and
other query parameters. A \class{jSpaceMarshaller} is initialized based on the
setting \class{GateFactory.LANGUAGE\_QUERY\_ELEMENT}. The mode of operation is
found in the query string, but as of now only \class{KEEP} is supported.
The method returns a new \class{KeepServerGate}.

\class{createClientGate()} works in the same way as \class{createServerGate}, but
initializes a \class{ClientGate} instead. \class{keep} mode is the only supported mode
and it returns a \class{KeepClientGate}.

A \class{GateFactory} controls the \class{GateBuilder}s and knows how to parse query
strings. The \class{getGateBuilder} method returns the builder corresponding to the
scheme you want.

\subsection{Introducing new Gates}

As discussed in \prettyref{ch:security} we will introduce secure
communication by applying TLS. To do so, we must alter the implementation to make
use of the \class{SSLSocket} class.
The current implementation applied the builder design pattern, which eases the
introduction of new \class{Gate} implementations. We can
keep the existing implementation and introduce a new class which inherits from
\class{ServerGate}, supporting TLS instead of a basic socket connection.
Due to object-oriented design, a TLS socket can be dropped in place of a TCP
socket with minor modifications.

\subsubsection{GateFactory}
It is necessary to let the \class{GateFactory} know about the new
\class{GateBuilder}s existence. It should be added to \class{init()} and a string
which is matched by parsing the URI must be defined as well.
The TCP gate builder was reusable for the TLS socket so specifying the protocol to be
\class{tls} will return this object.

\subsubsection{GateBuilder}
One must implement a new \class{GateBuilder} class. To successfully implement the
\class{GateBuilder} interface one must implement the methods:
\begin{lstlisting}
public ClientGate createClientGate(URI uri, Class messageClass);
public ServerGate createServerGate(URI uri, Class messageClass);
\end{lstlisting}
The \class{messageClass} parameter was added to make gates usable for different
classes of messages.

We will inspect the TCP implementation to get an idea of the functionality
necessary for such a class to function.

\paragraph{ClientGate createClientGate}
The URI is parsed and split and a marshaller is initialized and an instance
of the \class{ClientGate} class is returned, in this case \class{KeepClientGate}.

\paragraph{ServerGate createServerGate}
Does the exact same things as \class{createClientGate} but initializes and returns
a \class{KeepServerGate} instead.

\subsubsection{Gates}
Two \class{Gate} interfaces exists, \class{ClientGate} and \class{ServerGate}.

\paragraph{ClientGate}
A \class{ClientGate} has the methods
\begin{lstlisting}
void open()
Message send(Message)
void close()
\end{lstlisting}

The \class{KeepClientGate} is a bit more complex than the corresponding \class{ServerGate}
implementation. It introduces an inbox for incoming messages which uses
a Rendezvous synchronization mechanism\cite{concurrentbook}, and an outbox which is simply a
linked list.

\class{send(Message)} generates a new session ID from a session counter, sets
the target and session of the new message and adds the message to the
outbox and then notifies the outbox. It returns \class{inbox.call(sessionId)}
which effectively blocks the thread until a response is received.

\class{open()} creates a new instance of the \class{Socket} class and fetches the input and
output streams of the socket. It launches two new threads which handles the
in- and outboxes.

\class{outboxHandlingMethod()} simply waits for outbox to contain messages, and then
uses the marshaller to write messages to the writer.

\class{inboxHandlingMethod()} uses the marshaller to read incoming \class{Message}s.

\paragraph{ServerGate}

A \class{ServerGate} has the methods
\begin{lstlisting}
void open()
ClientHandler accept()
URI getURI()
boolean isClosed()
void close()
\end{lstlisting}

\subparagraph{Example}
\class{KeepServerGate} extends \class{TcpServerGate} which implements a
\class{ServerGate}.

\begin{description}
    \item[\sh{void open()}] opens a new \class{ServerSocket}.
    \item[\sh{ClientHandler accept()}] calls \class{getClientHandler} with
    \sh{ServerSocket.accept()} as a parameter.
    \item[\sh{URI getURI()}] constructs and returns a \class{URI} which fits the
    server properties.
    \item[\sh{getClientHandler()}] is implemented by \class{KeepServerGate} to return a new
        \class{KeepClientHandler} instance.
\end{description}

\subsection{Adding a TLS socket}

We already discussed adding a TLS socket to the \class{GateBuilder}. 
The \method{createServerGate} and \method{createClientGate} methods was
modified to handle the case where \class{tls} was used as the protocol.
It instantiates objects of the class \class{TlsKeepServerGate} and
\class{TlsKeepClientGate} respectively. Note that only \class{KEEP} mode is
supported by TLS gates.

The only difference in how the TCP and TLS work in Java is how the
\class{Socket} objects are instantiated, we defined a dedicated method
\method{createSocket} for this purpose. This allowed for subclassing instead of
implementing entirely new classes for the purpose.
\method{createSocket} uses the \class{SSLServerSocketFactory} and
\class{SSLSocketFactory} for creating the sockets in the TLS context.
