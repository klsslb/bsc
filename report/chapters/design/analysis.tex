\section{Analysis of the existing implementation}

The jSpace implementation is comprised of 3444 source lines of
Java code\footnote{Found using the SLOCCount Linux utility}. Most of the methods
did not contain comments which makes it more time consuming to read and understand
for people without prior knowledge of the project.

The following sections will summarize our analysis.

\subsection{Spaces and RemoteSpace}

Spaces are implementations of tuple spaces and they differ in the way
they handle operations and store data.
The developer documentation\cite{pspaces-dev} provides a list of
the available space implementations and the behavior of the operations.

A \class{RemoteSpace} is not an actual space. It is a wrapper that has a space
and a \class{Gate} which adds networking capabilities.

\subsection{Space Repository}

A space repository is used to organize and control external applications.
\begin{description}
    \item[\class{SpaceRepository}] acts as a server making spaces available
        over the network through a \class{Gate}.
    \item[\class{Gate}s] enables networking capabilities through a
        server-client model. An implementation of a TCP gate is currently the
        only option. The Builder pattern\cite{patterns} is used here with the
        intention of adding more options in the future.
    \item[\class{RemoteSpace}] acts as a client and supplies access to a tuple
        space held remotely in a space repository.
\end{description}

\subsection{Messages}
\label{sec:messages}
Messages are at the core of the protocol. Messages are structured data sent
between two
entities over a network connection and the message structure defines a set
of fields that are expected to be in such a message. This set of fields must at
least include what is necessary for pSpaces applications to work, and we
survey the existing message classes of jSpace to find what fields are needed.

jSpace defines two types of messages. A \class{ServerMessage} is a response that
is sent from a server to a client.
A \class{ClientMessage} is a request that is sent from a client to a server.
The two classes share most of their properties, and we will investigate the
possibility of merging the two message classes to improve the maintainability
of jSpace.

In the following subsections we will go through each of the message classes
and decide which fields are necessary to keep. We strive to keep the messages
as lean as possible and remove redundant data.

\subsubsection{The ServerMessage}

The \class{ServerMessage} has the following fields
\begin{description}
    \item[\class{ServerMessageType}] is the type of operation:
        options include \class{PUT}, \class{GET}, \class{QUERY} and
        \class{FAILURE}.
    \item[\class{InteractionMode}] is the mode of interaction to be used by
        the gate
    \item[\class{Status}] is a boolean for internal use.
    \item[\class{StatusCode}] is a HTTP response status code.
    \item[\class{StatusMessage}] is a response message to elaborate on the
        status code.
    \item[\class{Tuples}] is the list of tuples retrieved by handling the
        request, if any.
    \item[\class{ClientSession}] is a session identifier which is currently not used in
        the implementation.
    \item[\class{ServerSession}] is a session identifier which is crucial for the
        message handler to work.
\end{description}

\subsubsection{The ClientMessage}
The \class{ClientMessage} has the same fields as the \class{ServerMessage} in
addition to the following
\begin{description}
    \item[\class{ClientMessageType}] denotes the type of message and has the
        same options as \class{ServerMessageType} with the addition of
        \method{ACK} and \method{FAILURE}.
    \item[\class{Target}] is an identification string to target a specific space
        in a repository.
    \item[\class{Tuple}] contains a single tuple if applicable.
    \item[\class{Template}] contains a template used for querying and getting
        tuples.
    \item[\class{ClientURI}] is not in use in the current implementation,
        it is a `final` type and
        cannot be changed after object initialization. It is currently set to
        `null`.
    \item[\class{Blocking}] a boolean field to denote if the operation is
        blocking.
    \item[\class{All}] a boolean field to denote if all matching tuples or a
        single tuple should be returned.
\end{description}

\subsection{Response status codes and messages}
\label{sec:status}
When a space repository has received a request from a client and attempted to
handle it, it sends back a response. A response status code is attached to
the response to indicate wether the operation was executed successfully or if
something bad or unexpected happened.
jSpace uses a subset of the HTTP status codes in their status code field with
the addition of a message which describes what happened.
These codes are standardized\cite{statuscodes}, and \prettyref{tab:statuscodes}
summarizes the codes supported by jSpace, the message jSpace attaches and
the standardized meaning of the code.


\begin{table}[H]
    \centering
    \begin{adjustbox}{width=\textwidth}
    \begin{tabular}{llp{5cm}}
        \toprule
        \textbf{Code} & \textbf{jSpace message} & \textbf{HTTP meaning} \\
        \midrule
        200 & OK & The request was successfully received, understood, and accepted\\
        400 & BAD REQUEST &  The request contains bad syntax or cannot be fulfilled\\
        500 & INTERNAL SERVER ERROR & The server failed to fulfill an apparently valid request\\
        \bottomrule
    \end{tabular}
    \end{adjustbox}
    \caption{HTTP status codes used in jSpace along with the appended message
    and its standardized meaning. The codes in the table are the general form,
    and in the HTTP protocol they are usually found in a more specific
    subclassed form}
    \label{tab:statuscodes}
\end{table}

Including the status of an operation may increase the reliability of a user
application by enabling the user to make decisions based upon the status of
the server response.
We feel that the three status codes from \prettyref{tab:statuscodes} suffice
as a set of codes that will cover the errors we expect to handle now and in
the future. The code itself can be used to tell which class of error we have
to deal with, and customized messages can be used to further describe the
problem in order to make it easier for developers to decide what action needs
to be taken.

The status codes are intended to be used in the following scenarios
\begin{description}
    \item[200 OK] is used when nothing bad happens, i.e. the resulting state
        is as expected.
    \item[400 Bad Request] is used when the user is not authorized to use the
        requested resource or the message is malformed among others.
    \item[500 Internal Server Error] is used when the server encounters an
        exception and is unable to execute the desired operation.
\end{description}

\subsubsection{Interaction modes}

The user is able to choose how a gate should manage its connection.
pSpaces defines the following four modes. Out of these only \method{KEEP} and
\method{CONN} are used in jSpace. The authors of pSpaces would like us to
support \method{KEEP} as a minimum.

\begin{description}
    \item[\class{KEEP} (default)] Used when the connection must be persistent.
    \item[\class{CONN}] When method \class{CONN} is used, only one connnection
        is used and the request and its response are exchanged using the same
        connection.
    \item[\class{PUSH}] When method \class{PUSH} is used, the two messages are
        exchanged in two separate connections: the first opened by A and the
        second opened by B.
    \item[\class{PULL}] When method \class{PULL} is used, the two messages are
        exchanged in two separate connections: the first opened by A and the
        second opened by B.
\end{description}

\subsection{Unified message for space operations}
\label{sec:unifiedmsg}

From the set of fields found in \class{ServerMessage} and \class{ClientMessage}
described in \prettyref{sec:messages}
we will define a structure for the message that can replace both classes.

They both have a field for denoting the type of message so this is simply
merged into the field "type".

We intend to only support one mode of interaction, so we discard the
\class{InteractionMode} field.

The existing boolean \class{status} field is omitted since it is only used
internally and the status of an operation can be deduced from the status code
field, i.e. if the status code is 200, this field would be set to true, and
false otherwise.
The field is replaced with a \class{Status} object that holds a status code
and a message as described in \prettyref{sec:status}.

The fields \class{blocking} and \class{all} are removed since these can be
deduced from the \class{type} field which we extended with the "P" and "ALL"
types found in \prettyref{tab:messagestruct}.
We are convinced that it is clearer for the user if you can look at the type
and tell which operation it is.

The \emph{tuple} and \emph{template} of \class{ClientMessage} and
\emph{tuples} field of \class{ServerMessage} is merged into a
\emph{data} field. It makes no sense for a client to send a tuple and a
template in the same request, and the server only has the option of embedding
a list of tuples in its response.

The \emph{server session} field plays an important role in the implementation
of the message handlers. These use synchronization mechanism which relies on
the session. Since the \emph{client session} has no function it is removed and
server session is renamed to \class{session}.

The \class{target} field of the \class{ClientMessage} is used to target a
space in a \class{SpaceRepository}. We will introduce access control which will
be elaborated upon in \prettyref{ch:security}.
The access control system we introduce requires a set of keys to be included in
the message. These keys are specific to the targeted space so it is suitable to
include them here. \class{target} is converted into an object, holding the
necessary information for successfully targeting a space on the server.
The necessary information is the name of the space to be used in a repository
context, the set of keys authorizing the use of the space and an indentifier
which is assigned by the server when the space was created. This identifier is
unique and is used when a space is attached to a repository.

The \class{ClientURI} field does nothing and is discarded.


\subsection{Summary of the message fields}

\prettyref{tab:messagestruct} summarizes the fields of the message structure
we defined in \prettyref{sec:unifiedmsg}
It covers both messages sent from a client to a server and in the opposite
direction.

\begin{table}[H]
    \centering
    \begin{tabular}{ll}
        \toprule
        \textbf{Field} & \textbf{Value} \\
        \midrule
        Status & Object with a code and message \\
        Session & Session counter \\
        Type & \method{PUT}, \method{GET}, \method{GETP}, \method{GETALL},
            \method{QUERY}, \method{QUERYP}, \method{QUERYALL},
            \method{ACK}, \method{FAILURE} \\
        Target & Target space information (name, keys, ...) \\
        Data & A single tuple, single template or list of tuples \\
        \bottomrule
    \end{tabular}
    \caption{Summary of fields in the message used for space operations
        and the values that they may be set to.}
    \label{tab:messagestruct}
\end{table}

\section{A proposal for a server-client model}

In order to reduce the amount of work it takes to produce and maintain an API
written in another language, we propose to split the functionality into a server
and a client.

The server will be holding the spaces, repositories and tuples. It will accept
requests sent by clients and execute the desired operations. The server is the
only instance that knows how to execute the operations so it performs all the
heavy lifting. The server application is not meant to be altered by the user.
The server will have multiple gates available. It will have one for managing
the creation of repositories and spaces on the server.
Each repository created on the server will have its own gate. This gate
expects requests for operations on tuple spaces kept in the repository.
Configuration of the server is done at run-time through a configuration file,
removing the need of altering the source code itself. \prettyref{sec:serverconfig}
will summarize what options can be altered by the users.

The client needs to be able to connect to the server through a gate
and marshal messages to a format the server understands.
Implementing a client API that adheres to our model
requires the developer to learn the structure of the
messages used for space operations and managing the server,
and additionally implement a marshaller that the server supports.
This may greatly simplify
the implementation of a client, as it is no longer necessary to understand and
implement the tuple spaces and repositories.

It is the client API that regular users will develop their applications with,
and as previously stated the client API will stay as close as possible
to the original.

A performance overhead is to be expected, as all operations are converted
to messages and sent through a gate to another application before it is
executed. In the original approach it was also possible to
execute operations directly on a space without having to embed it in a
message and sending it through a gate.
If the tuple spaces were to be used in a distributed context it would
depend on how the system is used. If only a few operations are executed locally
the overhead will be negilable.

By extracting the core server functionality from jSpace into a self-contained
application, it gives rise to the introduction of an additional message
structure: a message exchanged between the server managing the repositories and
spaces and the client.

\begin{figure}[H]
    \centering
    \input{chapters/design/comm-diagram-old-fig.tex}
    \caption{The architecture of jSpace as it works now. Client 1 and Client 2
        are both using the jSpace library. Clients written in e.g. goSpace is
        not able to communicate with jSpace entities because of inconsistencies
        in the protocol.
        The text in the blue box is Java code. Text in angle brackets
    represent the type field of the message.
    In this example the space repository is listening on port 9001}
    \label{fig:oldarch}
\end{figure}

\subsection{A message structure for managing the server}

The server-client model introduced in the previous section requires a message
structure to be defined for managing the server and its contents. It is no
longer possible to instantiate a repository and space objects directly in a
Java program, it is now done by sending special requests to the server managing
the repositories.

The status, session and space properties fields have the same meaning as
in the message structure defined in \prettyref{sec:unifiedmsg}.

The type field denotes the type of operation and currently includes options
to create a repository, create a space and attach a space to a repository.

The access control system requires that a key for managing the server is sent
with the message. The key is stored in the \emph{server key} field.

In order for the server to know which repository it should operate on or create,
a set of \emph{repository properties} needs to be included in the message.
These properties includes a name, a key and a TCP port.

A complete summary of the fields this type of message will have is seen in
\prettyref{tab:srvmsg}.

\begin{table}[H]
    \centering
    \begin{tabular}{ll}
        \toprule
        \textbf{Field} & \textbf{Value} \\
        \midrule
        Status & Object with a code and message \\
        Session & Session counter \\
        Type & \method{CREATE\_REPOSITORY}, \method{CREATE\_SPACE}, \method{ATTACH} \\
        Space & Target space information \\
        Repository & Target repository information \\
        Server key & The key used for managing the server\\
        \bottomrule
    \end{tabular}
    \caption{Summary of the fields found in the server management message and
        the values that they may be set to.}
    \label{tab:srvmsg}
\end{table}

\subsection{The revised system architecture}

\prettyref{fig:newarch} shows the intended behavior of two clients
interacting with a server instance in the architecture. \prettyref{fig:oldarch}
shows the same procedure, but with the current unaltered jSpace library in use.
With the new architecture two clients may interact in the following way.

Client 1 connects to the server and requests the creation of a repository by
sending a message to the server. Since the repository does not yet exist the
server creates the repository while initializing a gate for it using a port it
selects at random, in this case port 9001 was selected. It responds to the
client with the port that the repository is listening on.
Client 1 is now able to initialize a connection to the repository, but a space
will still need to be created. in jSpace, spaces are created with a method
on a repository object. This method will send a message to the management
server requesting that the new space is created. The repository information is
included in the message, and if the information contained in the message is
correct, the server will create the space and attach it to the repository.

Client 2 connects to the server in the same fashion as Client 1, and requests
the creation of a repository with the same name. Since it exists, the server
simply responds with port 9001, assuming that the supplied server management
key and repository keys are
correct. The client creates a space, but since the space already exists, the
server does nothing except sending back an "OK" response.

Client 2 now attempts go get a tuple from the space. The space is empty at the
moment, so the client will be blocked by waiting for a response from the
server.

Client 1 puts the tuple that Client 2 expects into the space, and now the
server responds to Client 2 which lets it exit its blocked state.
\begin{figure}[H]
    \centering
    \input{chapters/design/comm-diagram-fig.tex}
    \caption{Architecture using the new server-client model. The server and
        clients are running in separate processes. The text in the
    blue boxes is the client code, in this case Java, but any client adhering to the
    protocol could be used. Text in angle brackets
    represent the type field of the message. In this example the management
    server is listening on localhost, port 7000 and the repository was given
    port 9001 by the server.}
    \label{fig:newarch}
\end{figure}




% Second idea: Have a single server process have multiple repositories to avoid
% interprocess communication.
% On creation of a new repository one could set repository wide settings like
% encryption and compression.
% This could also be set per tuple space instead.
% 
% A space repository could either be indentified by a port, or by a name (kept
% in a hashmap for easy access). The latter will give us more (infinite) space
% for repositories as we are not limited by number of available ports.

%  XXX we should probably reuse this list
% \subsubsection{Potentially bad situations}
% \begin{itemize}
%     \item Adding a space or repository with a name that already exists
%     \item Removing a space with a name that does not exist
%     \item Adding a space or repository using the wrong management key
%     \item Attempting to pair/unpair repository and space with wrong key
%     \item Attempting to pair/unpair repository and space with wrong names
%     \item Attaching a space
% \end{itemize}
