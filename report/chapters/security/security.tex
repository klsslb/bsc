Being a distributed application with potentially multiple users using the
same server, authorization provides security advantages.
Without any form of authorization, any user may modify the server,
repositories and spaces.
A user is potentially malicious, and may attempt to give other users a bad time.
This can be avoided by introducing an access control system, so that users
need to be authorized in order to read or write any data on the server.

\section{Authorization}

The authorization system is built using pre-shared keys, meaning that
a user starting the server knows what key it is configured with through its
configuration. Using the default password is insecure as it is an empty string
and should be changed to protect the server owner from unwanted users. Another attack
surface is the configuration file which is stored in plain-text on the disk,
which is bad practice for files containing sensitive data.

Other keys involved in the protocol are eventually going to be stored on the
disk, since these need to be included in users' client programs.
Publicly sharing programs, which is a common practice among users like students,
means that they will not remain secure unless keys are removed before sharing
their source code. Possible solutions to these problems are
brought up in \prettyref{sec:future}.

\subsection{Authorization keys}
A brief summary of the authorization associated to the keys involved in the protocol
is listed in \prettyref{tab:keytable}. If a malicious user knows the server key he
can connect to the server and overload it by i.e creating repositories until it
hits memory limitations.
\begin{table}[H]
    \centering
    \begin{adjustbox}{width=\textwidth}
    \begin{tabular}{lp{8.5cm}}
        \toprule
        \textbf{Key} & \textbf{Authorizes} \\
        \midrule
        Server key & Creation of space, space repository. \\
        Repository key & Creation of a space, management of a space repository. \\
        Space key & Management of a space. \\
        Put, Get, Query keys & Basic operations on a space associated with the keys. \\
        \bottomrule
    \end{tabular}
\end{adjustbox}
    \caption{Authorization key types and what they authorize the holder to do}
    \label{tab:keytable}
\end{table}

Holding the server key does not grant access to actual data in a tuple space,
since these are protected with their own key. Getting hold of these keys
requires that the traffic is eavesdropped, a technique known as \emph{Man-in-the-middle}
or MITM attack which we will protect against by encrypting the message channel.

%\subsection{Space operations}
%At first we thought about having one key for writing a space and one key for
%reading from a space. A combination of these keys would then give permission
%to execute the various operations. This system did not seem intuitive and we
%replaced it with a similar system. One key is needed for put operations,
%one is needed for get operations and one is needed for query operations.
%This way, administrators have a clear and easy way to verify which users are
%authorized to execute specific operations.

\section{Encrypting the message channel}
Incorporating user access control into the protocol design means that
sensitive data like the various keys are sent over the network. If an attacker
were to eavesdrop on the communication between the server and a client
he could potentially get hold of the server key through a MITM attack.
A holder of the server key are capable of making the lives of users
miserable, which is something that we wish to avoid.

We want encryption of the messages enforced by default, but with a possibility
to disable it. The original jSpace implementation had no security measures at all,
and everything was sent in plain-text.
Although it was designed to also run locally, it had networking capabilities.

We have 3 components that we can apply encryption to and different options
of doing so.

\begin{enumerate}
    \item Communication channel between the client and the main server.
    \item Communication channel between the client and a repository instance.
    \item Tuples stored in spaces.
\end{enumerate}

1. and 2. can be solved by applying TLS to the TCP connection or by using
an application key together with some encryption algorithm, e.g. AES.
The option of using an application key comes with some advantages in certain
scenarios which we will discuss in \prettyref{sec:aes}.

3. might be solved by introducing a new space type, e.g. \class{EncryptedSpace}.
But it is going to be a challenge to search for tuples in an encrypted space.
Tuples will have to be decrypted before their fields can be checked.

\todo{Consider hashing of keys before they are sent}

\subsection{Application key in conjunction with AES}
\label{sec:aes}

Introducing an application key could be fine in a single-user context where the
key isn't shared.
But if a malicious user were to get hold of the application key, that user is able
to decrypt all communication between the server and the clients.
If the malicious user can
intercept the requests that creates spaces and repositories, their respective
keys could potentially get stolen.
The main flaw of this approach lies in a single key being used for encrypting
all communication channels. It gives rise to concerns about how the key is
handled by the end user, if it is shared or too simple it is trivial to bypass
the encryption.

A suitable encryption algorithm to use in conjunction with an application key is
AES, which has hardware acceleration. Hardware acceleration will reduce the
encryption overhead introduced by a significant factor.

The benefits of using AES over TLS for encrypting the message channels includes
reduced complexity for the user by not having to generate and hold certificates.
AES is a more flexible choice by not being limited to a transport protocol like TCP.

\subsection{Transport Layer Security}
\emph{Transport Layer Security} (TLS) is the successor to \emph{Secure Sockets Layer} (SSL).
As with SSL, it is a protocol designed to provide encryption of data sent over
a TCP connection using symmetric key cryptography.

TLS is widely used on the internet by being applied in HTTPS,
and many programming languages have a TLS implementation in their standard library.

Java 7 and beyond implements TLS version 1.2 which is the current standard.
It is the default version used beginning with Java 8\cite{java8tls12}.

\subsubsection{TLS Handshake}
A summary of how the TLS protocol establishes a secure connection is important.
Users of the system we designed will manually perform a key element of the secure
protocol setup, which is known as the TLS handshake, and embed it in their programs.
Therefore, understanding where and how they are participating is relevant in a
security context.

In symmetric key cryptography "the encryption of a plain-text and the decryption
of the corresponding cipher-text is performed using the same key, that is, the
sender and receiver share the same key."\cite[p. 7]{cryptobook} This concept
is used in TLS.
Since two entities have to agree on a symmetric \textit{secret key} before a
communication channel is deemed secure, also known as the key-agreement problem,
TLS also utilizes asymmetric cryptography during the handshake, to generate and
exchange such a key in a secure manner. This crucial part can be achieved using
i.e Diffie-Hellman key exchange protocol.\cite{openssl}
A variety of solutions to the key-agreement problem under TLS exists and the
parties must agree on a cipher during initial steps of the handshake.

The generated key will be used for the entire session to encrypt communication
using an agreed symmetric cipher also determined in the handshake.

But before any keys can be generated and exchanged the two parties have to validate
their authenticity and integrity. A common but vital parameter of secure
communication, which ensures that the sender and receiver are who they claim to
be and that the exchanged data are not tampered with.
In TLS this is achieved by sharing certificates which contains information about a
participant like name and public key. The certificate has to be signed by a trusted
third party called Certificate Authorities (CA). One can think of a CA as a vouching
entity of certificates. In our system this is not an automated process. Users
are responsible for managing their certificates and signing them.

A detailed step-wise process of how the handshake safely initializes a secure
channel is beyond the scope of this document
but a simplification, that summarizes the components discussed above,
can look like the following:
\begin{itemize}
    \item Communicating parties agree on protocol for key exchange
    \item Prove authenticity of identification in form of digital certificates
    \item Agreement of cipher specification used in secure channel
\end{itemize}
if nothing fails the handshake is considered complete and communication using
TLS is possible.


\subsection{Self-signed certificates}

As mentioned above, gaining the benefits of TLS means validating certificates by a
trusted third party authority. And the tool we used to circumvent this, albeit
useful for testing scenarios and basic setup, is not recommended for 
production use.

Keytool is an application distributed with the JDK to generate certificates and
also sign them. So when utilized, the users are essentially responsible for
managing their own public and private key pairs associated with the generated
certificates, which they can also self authorize using the digital signatures.
\cite{java8keytool}
Both created certificates and signed certificates are stored
in password protected files issued by the user and created by Keytool.
These files are commonly known as \emph{keystores} and if not generated along with a
certificate which is signed and imported, the handshake will fail.

The Keytool documentation is comprehensive and we
decided to make an easy getting-started guide based on the essential commands
needed for users of our implementation. It can be viewed online in the
documentation we made but will also be explained below.

First create a keystore called \class{myKeyStore.jks} in current path and generate a
certificate, called \class{myKey}, with a key pair based on the RSA algorithm using 2048
bits with validity of 100 days.

\begin{lstlisting}[caption=Using the Keytool for generating a key store]
keytool -genkeypair -keyalg RSA -keysize 2048 -validity 100 -alias \
mykey -keystore myKeyStore.jks
\end{lstlisting}

Extract the now signed certificate from the keystore just created and name it
\class{myKey.cert}, preparing it for import to a trusted keystore not yet generated.

\begin{lstlisting}[caption=Extracting the signed certificate]
keytool -export -alias myKey -keystore myKeyStore.jks -file myKey.cert
\end{lstlisting}

Create the trusted keystore called \class{myTrustStore.jks} and import the certificate.

\begin{lstlisting}[caption=Creating a trusted key store and importing the certificate]
keytool -import -file myKey.cert -alias myKey -keystore myTrustStore.jks}
\end{lstlisting}

This concludes the basic setup providing the necessary parameters for TLS
communication in the Java environment. The commands are bundled in a \class{Makefile}
to further increase user experience.


\section{Performance and constraints}

By enforcing a security layer we also introduce a performance overhead.
The architecture of modern desktop processors include a special set of instructions
for boosting encryption and decryption performance, when using common
encryption algorithms.\cite{aesni}
We briefly touched on this subject in \prettyref{sec:aes}. The hardware
acceleration will benefit both AES in conjunction with an application key and
TLS if the AES cipher suite is used.

So defaulting encryption might not be a big deal for desktop processors, but if
we are looking at using the library in sensor network applications, the
performance might suffer significantly. Processors used in sensor networks are usually
simpler and has less features than a fully-fledged desktop processor. This
reduces its cost and energy consumption, but might also mean that it does not
come with special instructions for encrypting data.

In a distributed computing context, clients are not necessarily human beings
and the above scenario is perfectly plausible. A client could potentially
be one person running a server for such a sensor network with a large number of
clients. The option of a single application key, encrypting messages
using a lightweight cryptography cipher, would be perfectly fine in this context.

It could also be groups of students taking a course in distributed systems.
Here it could potentially introduce exposure for malicious use if the same
application key is used for the shared server. To overcome this problem,
other than with TLS, a solution could be to use asymmetric cryptography where
the server manager is responsible for the private key and users are forced
to encrypt their messages with the public key.

One of the benefits of using AES is the capability of efficiently implementing
it on 8-bit microcontrollers.\cite{aesbook}
This class of microcontrollers might see use in a sensor network and they
typically  have severe resource constraints.

A measurement of performance overhead was not a priority in this report, but
it is definately interesting to look into in the future.
