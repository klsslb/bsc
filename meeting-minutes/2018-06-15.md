# Meeting 15 June 2018

Present: Alberto, Kasper, Sebastian

We went through the Wiki we shared with Alberto, and told him about our progress
so far.

He added:

- Double check everything on the Wiki for inconsistencies. At least some
  Java examples have `createSpace` (should be `addSpace`). Consider scrapping
  the tables.
- Scrap the current marshaller and use Thomas'
- Enforce encryption of at least the server key
- Enable TLS encryption (option or enforce, we choose!)
- Base64 encoding of large binary data. How are tuples with array types
  handled?
- Go through *Open questions* and maybe change them to optional things.
  Decide whether or not to keep them.
- Revise the user guides and create a tutorial for new things to make Alberto's
  life easier.
- Either in *Overview* or *Server management* we need to be more explicit or
  detailed in our explanations. We are lacking in this section.
- Design an interface for authorization, which makes it easy to control
  how it is achieved in the future. One method for authorizing each of the
  operations.
