\documentclass[aspectratio=43]{beamer}

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc} \usepackage[english]{babel}
\usepackage{pgfplots}
\pgfplotsset{compat=newest}
\usepackage{booktabs}
\usepackage{siunitx}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{xcolor}
\usepackage{MnSymbol,wasysym}
\usepackage{tikzsymbols}
\usepackage{tabularx}
\usepackage{adjustbox}
\usepackage{pgfplotstable}

% macros
\newcommand{\sh}[1]{\colorbox{lgray}{\texttt{#1}}}
\newcommand{\class}[1]{\texttt{#1}}
\newcommand{\method}[1]{\texttt{#1}}

% Latin Modern
\usepackage{lmodern}
% Verdana font type
%\usepackage{verdana}
% Helvetica
%\usepackage{helvet}
% Times (text and math)
%\usepackage{newtx, newtxmath}

\usetheme[department=compute]{DTU}

\title[pSpaces Protocol]{Unified Protocol for pSpaces libraries}
\author{Kasper Lindegaard Stilling and Sebastian Lindhard Budsted}
\institute{B.Sc. Project}
\date{\today}

\newcommand{\tabitem}{{\color{dtured}$\bullet$} }

\begin{document}
\frame{
	\maketitle
}

\frame{
	\frametitle{Today's agenda}
	\tableofcontents
}

\section{Introduction} % Discuss suitable location
\frame{
    \frametitle{The pSpaces project}
    \begin{itemize}
        \item A framework for programming distributed applications with tuple spaces
        \item Has libraries for some mainstream languages: Java, C\#, Go, plus more
        \item Exhange of data between current APIs is not possible at the moment
        \item Shared protocol enables the exchange of data between APIs
        \item Still under active development; expect more languages to be supported
        \item A protocol specification will ensure that communication is possible, if adopted
    \end{itemize}
}
\section{Problem description}
\frame{
    \frametitle{Problem statement}
    \textbf{Problem statement:}
    Design a unified protocol for the pSpace libraries with all the necessary
    documentation to ensure that the different libraries can extend while adhering
    to the mutual protocol. Implement the protocol in the Java library jSpace and
    provide basic documentation for the end-user.
}
\frame{
    \frametitle{Problem requirements}
    \textbf{Requirements:}
    \begin{itemize}
        \item Design a communication protocol for pSpaces APIs
        \item Implement the design in jSpace
        \item Provide developer documentation for the protocol
        \item Test implementation of the protocol in jSpace
        \item Provide tutorials for client API
        \item Ensure that existing examples can run using new implementation
    \end{itemize}
}
\section{Tuple spaces} % To give an overview of what we need to support
\frame{
    \frametitle{What is a tuple space}
    \begin{itemize}
        \item A Collection of tuples held in a shared data structure
        \item Tuples can be thought of as lists containing data elements
        \item Can add tuples using a PUT operation, non-blocking
        \item Can inspect tuples using QUERY operations, blocking
        \item Can remove tuples using GET operations, blocking
        \item Non-blocking versions of GET and QUERY are available by adding 'P'
        \item 'ALL' can be added to retrieve all matching tuples
    \end{itemize}
}
\section{Design}
\frame{
    \frametitle{jSpace library}
    Serves as the reference implementation of a pSpaces library.
    The current implementation has:
    \begin{itemize}
        \item support for all tuple space operations
        \item A series of space implementation with different semantics
        \item The notion of space repositories; collection of tuple spaces
        \item Networking capabilities through 'Gates' (TCP only at the moment)
        \item An undocumented and incomplete protocol with redundant fields
        \item A simple JSON marshaller and a complex BinaryMarshaller
        \item The means to concurrently handle multiple requests
    \end{itemize}
}
\frame{
    \frametitle{Protocol requirements}
    The protocol design should support the following features, where some have
    not yet been introduced:
    \begin{itemize}
        \item Tuple space operations
        \item The newly introduced access control system
        \item Secure communication through TLS
        \item Management of spaces and repositories held remotely at a server
    \end{itemize}
%        \item Inspired by the functionality of jSpace
}
\frame{
    \frametitle{pSpace messages}
    A pSpaceMessage is sent between clients and repositories
    It carries the necessary information for executing a space operation
\begin{table}[H]
    \centering
    \begin{tabular}{ll}
        \toprule
        \textbf{Field} & \textbf{Value} \\
        \midrule
        Status & Object with a code and message \\
        Session & Session counter \\
        Type & \method{PUT}, \method{GET}, \method{GETP}, \method{GETALL},
            \method{QUERY}, \method{QUERYP}, \method{QUERYALL},
            \method{ACK}, \method{FAILURE} \\
        Target & Target space information (name, keys, ...) \\
        Data & A single tuple, single template or list of tuples \\
        \bottomrule
    \end{tabular}
    \label{tab:messagestruct}
\end{table}
}
\frame{
    \frametitle{Message for managing the server}

    A ManagementMessage is sent between clients and the management server
    It carries information for creating repositories and spaces
\begin{table}[H]
    \centering
    \begin{tabular}{ll}
        \toprule
        \textbf{Field} & \textbf{Value} \\
        \midrule
        Status & Object with a code and message \\
        Session & Session counter \\
        Type & \method{CREATE\_REPOSITORY}, \method{CREATE\_SPACE}, \method{ATTACH} \\
        Space & Target space information \\
        Repository & Target repository information \\
        Server key & The key used for managing the server\\
        \bottomrule
    \end{tabular}
    \label{tab:srvmsg}
\end{table}
}

\frame{
    \frametitle{Documentation}
    \begin{itemize}
        \item Serves as a guide for developers adopting the protocol
        \item Available online as GitLab Wiki pages
        \item The Wiki pages are written in Markdown; easy to understand and learn
        \item Should be concise but still have the neccesary information and be easy to understand
        \item Existing documentation should be adapted to fit the new implementation
    \end{itemize}
}
\section{Security}
\frame{
    \frametitle{Access control}
    \begin{itemize}
        \item Provides means to have private spaces
        \item Exclude potentially malicious users from tampering with a space
        \item Necessary in a networked context where multiple users use the same server
    \end{itemize}
\begin{table}[H]
    \centering
    \begin{adjustbox}{width=\textwidth}
    \begin{tabular}{lp{8.5cm}}
        \toprule
        \textbf{Key} & \textbf{Authorizes} \\
        \midrule
        Server key & Creation of space repositories \\
        Repository key & Creation of spaces and management of the space repository \\
        Space key & Management of the space (attachment to a repository) \\
        Put, Get, Query keys & Basic operations on a space associated with the keys \\
        \bottomrule
    \end{tabular}
\end{adjustbox}
    \label{tab:keytable}
\end{table}
}
\frame{
    \frametitle{Transport Layer Security}
    \begin{itemize}
        \item Provides encryption for the message channel through multiple available cipher suites
        \item Simple to implement; replace the current Socket objects with SSLSockets
        \item Easy to add to the existing Gate implementations while keeping TCP support
    \end{itemize}
}
\frame{
    \frametitle{Network architecture of the current implementation}
    \begin{figure}
        \includegraphics[scale=0.9]{../figures/comm-old.pdf}
    \end{figure}
}
\frame{
    \frametitle{Network architecture of the revised implementation}
    \begin{figure}
        \includegraphics[scale=0.9]{../figures/comm-new.pdf}
    \end{figure}
}

\section{Demo}
\frame{
    \frametitle{Chat example}
    Time for a small demonstration...
}

\section{Conclusion}
\frame{
    \frametitle{Strengths}
    \begin{itemize}
        \item Documentation is available which eases the adoptation of the protocol
        \item Server-client model reduces the amount of work required when writing client APIs
        \item Added security layer through the access control system
            \begin{itemize}
                \item Control which users can execute specific operations
                \item Exclude users from using some spaces
            \end{itemize}
        \item Added security layer through TLS
            \begin{itemize}
                \item Adds encryption to TCP connections
                \item Can ensure that messages sent are not tampered with by 3rd parties
                \item Possible to verify authenticity of the server through certificates
            \end{itemize}
    \end{itemize}
}
\frame{
    \frametitle{Weaknesses}
    \begin{itemize}
        \item jSpace is not thoroughly tested
        \item Added security through TLS forces users to handle certificates
        \item TLS might not be applicable for constrained applications
        \item Security aspect puts responsability in the hands of users
        \item Number of repositories is still limited by the number of available ports
        \item Marshallers appears to be rather complex to implement
    \end{itemize}
}
\frame{
    \frametitle{Known issues}
    \begin{itemize}
        \item JavaDocs are currently lacking and existing docstrings have bad references
        \item 'ALL' operations needs minor modifications before they actually work
        \item Exception handling could be better
        \item The developer documentation needs to be updated
        \item Access control lackes support for users without keys
    \end{itemize}
}
\frame{
    \frametitle{Future perspectives}
    \setbeamercovered{transparent}
	\begin{itemize}
        \item Create a complete test suite for jSpace
        \item Support a raw AES encryption layer
        \item Investigate potential gains from applying compression
        \item Investigate if certificate handling can be abstracted away
    \end{itemize}
}

\section{Questions?}
\frame{
    \frametitle{Questions?}
}

\end{document}
