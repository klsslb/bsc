# Unified Protocol for pSpaces libraries
This is our BSc project. We will create a unified protocol for the existing
libraries, and implement the changes in the Java library *jSpace*.

## Relevant links
- [pSpaces GitHub user](https://github.com/pSpaces)
- [Public jSpace repository](https://github.com/pSpaces/jSpace)
- [jSpace examples](https://github.com/pSpaces/jSpace-examples)
- [goSpace](https://github.com/pSpaces/goSpace)
- [Site for course 02148 (main user of pSpaces)](https://gitlab.gbar.dtu.dk/02148/home)
- [Working with private git forks](https://stackoverflow.com/questions/10065526/github-how-to-make-a-fork-of-public-repository-private)
- [Wikipedia: Protocol](https://en.wikipedia.org/wiki/Protocol_(object-oriented_programming))
- [Wikipedia: Marshalling](https://en.wikipedia.org/wiki/Marshalling_(computer_science))
- [Wikipedia: Serialization](https://en.wikipedia.org/wiki/Serialization)
- [Writing good JavaDocs](https://stackoverflow.com/questions/5401636/how-to-write-good-javadoc-comments)
- [E2E Testing](https://stackoverflow.com/questions/23166297/how-to-write-java-e2e-end-to-end-tests)
- [More on integration testing](https://zeroturnaround.com/rebellabs/the-correct-way-to-use-integration-tests-in-your-build-process/)
- [UML Diagrams](https://perso.ensta-paristech.fr/~kielbasi/tikzuml/var/files/doc/tikzumlmanual.pdf)


## Figures

Figures created for the Wiki and report are found in the `figures/` directory.
Check out the `Makefile` for the targets you are interested in.

Please make sure that the creation of your figure is automated with `make` and
the figure is put in a suitable directory.

## jSpace

### Getting code coverage reports

Running the tests and generating a code coverage report can be done using
Gradle. Make sure you are in the `<jSpace repo>/common/` dir, then execute
```
$ gradle test jacocoTestReport
```
to generate a report. The report is found in
`<jSpace repo>/common/build/jacocoHtml/index.html`.
