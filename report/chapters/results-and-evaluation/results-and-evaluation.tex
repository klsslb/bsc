\chapter{Results and evaluation}
\label{ch:results}

In \prettyref{sec:requirements} we presented a list of requirements that
we will use to evaluate if the project has been successful.
\begin{description}
    %Design a communication protocol for pSpace APIs.
    \item[R1.] A design for a unified communication protocol intended for
        use with pSpace APIs has been created.
    %Implement the design in the Java library jSpace.
    \item[R2.] A version of jSpace has been implemented communicating using
        the new unified protocol successfully, but the functionality of
        some space operations have yet to be implemented.
    %Provide the documentation needed for implementing the protocol
     %other pSpace libraries.
    \item[R3.] Documentation of the unified protocol intended for library
        developers is available online for review. JavaDocs can be generated
        by using the build script included with the source code.
    %Test the implementation of jSpace using the jUnit library.
    \item[R4.] The protocol package of jSpace was tested using JUnit with
        decent test coverage of instructions.
    %Provide tutorials for the client API.
    \item[R5.] A simplified usage example of the client API can be seen in \prettyref{fig:newarch}.
        A series of usage examples can be found in the online documentation found at
        \url{https://gitlab.com/klsslb/bsc/wikis/usage-examples}. They are unfortunately
        not up to date at the time of this writing, but the intention is that
        these will serve as user tutorials.
    %Run the existing 'getting-started' examples using the new implementation.
    \item[R6.] The 'getting-started' examples have not yet been adapted to the
        changes in the API. This is mainly because some of the space operations
        have not yet been implemented. As with \textbf{R5}, these will be
        brought up to date.
\end{description}

\section{Testing the library}
\label{sec:testingthelib}

To ensure that the protocol is conforming to the unified protocol, a series of
unit tests was constructed for validation.
In a course in software engineering\cite{swengcourse} we were introduced to
automated testing, which aids developers
in ensuring that their changes do not break functionality in the library.
Automated testing also allows for \emph{code coverage reports}
to be generated.
Code coverage reports tell how much of the application code was
executed when the test suite was run.
They can be used to see what parts of the program are missed by the
existing tests, and where improvements can be made.
Although being desirable, it is difficult to get 100\% code coverage.
The \emph{JaCoCo} library was chosen to fill this purpose.
\prettyref{sec:coverage} summarizes the coverage we obtained.

The goal is to achieve a good automated test coverage of the new additions to
the library. Time will not be spent on filling the existing test gaps.

Many APIs exist for unit testing in Java. We chose to keep using the library
which is currently used in the project: JUnit. We updated the version from 4.8,
which was relatively old (4.12 was released in March 2016), to version 5.
JUnit 5 includes a package \emph{JUnit Vintage} which supports unit tests written
in version 3 and 4 of the library.
New tests will be written for the new \emph{JUnit Jupiter} library.

We discussed converting the current tests to keep the different library
versions to a minimum which may reduce the amount of work for future
developers. Time did not allow for this to happen.

The JUnit 5 migration guide\cite{gradlemigrate}
was used to modify the Gradle build script to
include support for both JUnit 5 and JUnit vintage versions.

As discussed in \prettyref{sec:tdd} we follow a test-driven development approach
where we define the tests first, and then
implement the classes and methods to make the tests pass.
TDD was only applied to the protocol design, and not the changes to the overall
architecture of jSpace. The existing library was lacking tests in the first
place, and we had difficulties testing the networked operations due to blocked
threads making the test-runner hang.

Besides unit testing of the protocol classes, small programs loosely following
the user scenarios found in the online user documentation\cite{our-gitlab} to
test that the functionality was working as expected.

The external \class{EqualsVerifier} library was used to thoroughly test the
\method{equals} and \method{hashCode} methods, instead of manually writing these
extensive tests. It was included after spending hours trying to fix a bug in
the code which appeared when the existing tests did not cover some newly
introduced fields of a class.


\subsection{Code coverage reports}
\label{sec:coverage}

\begin{figure}[H]
    \includegraphics[width=\linewidth]{figures/jacoco-test-results.png}
    \caption{An overview of the failed unit tests. The failures are due to
        the string representation used. "Prettyprinting" has been enabled for
        GSON, but it is not the format that is expected by the tests.}
    \label{fig:jacoco-summary}
\end{figure}
\prettyref{fig:jacoco-summary} shows that 97\% of the tests are passing. This
number can be misleading, as most of the project currently lacks unit tests.

\begin{figure}[H]
    \includegraphics[width=\linewidth]{figures/codecoverage.png}
    \caption{Code coverage report showing the statistics for all packages
    contained in the repository. We are interested in the code coverage of the
    protocol and config packages only.}
    \label{fig:codecoverage}
\end{figure}

\begin{figure}[H]
    \includegraphics[width=\linewidth]{figures/protocol-coverage.png}
    \caption{Test coverage of the classes contained in the protocol package.}
    \label{fig:protocol-coverage}
\end{figure}
\begin{figure}[H]
    \includegraphics[width=\linewidth]{figures/config-coverage.png}
    \caption{Test coverage of the classes contained in the config package.}
    \label{fig:config-coverage}
\end{figure}

\prettyref{fig:codecoverage} gives an overview of the entire source code
repository. Most of the packages in the report have not been implemented by
us. These packages are lacking tests if good coverage is to be achieved.

\prettyref{fig:protocol-coverage} shows the code coverage of the new
unified protocol. The coverage is not 100\%, but we have since the
implementation fase found ways to improve this number.

\prettyref{fig:config-coverage} shows the coverage of the config package which
is a new addition to the library. The package has good coverage.

\subsection{TLS Verification}

A Wireshark instance listening on the loopback interface of a testing machine
was used to verify that data was encrypted using the TLS protocol. Inspecting
the 'Protocol' column along with the 'Info' column shows that the application
data is succesfully sent using TLS.
\prettyref{fig:wiresharkdump} displays the some of the corresponding packets
when running a basic example performing PUT requests.
\begin{figure}[H]
\includegraphics[width=\linewidth]{figures/wireshark-dump.png}
    \caption{A screenshot of relevant packets captured with Wireshark.}
    \label{fig:wiresharkdump}
\end{figure}
The certificate created with Keytool had to be used during the handshake, otherwise
the setup would fail. But to insure its usage we can inspect a packet sent from
the server during the handshake, and confirm some of the properties. A property
we set with Keytool was the certificate validity of 100 days.
In \prettyref{fig:packet-info} a packet is inspected and the details show certificate
properties like validity that matches the one we created.

\begin{figure}[H]
\includegraphics[width=\linewidth]{figures/wireshark-cert-validity.png}
    \caption{A screenshot of a single packet information captured with Wireshark
    where the certificate properties is inspected}
    \label{fig:packet-info}
\end{figure}
