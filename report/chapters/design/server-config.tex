\section{Server Configuration}
\label{sec:serverconfig}

The server application code is not intended to be modified by users, but in
some cases they might want to change the default options before the application is run.
Having the server load its configuration at run-time allows for the
configuration defined by the server developers to be changed by the users.

The server has hard-coded defaults which can
be overridden by options defined in the configuration file. The configuration
options defined in the file can be overridden by command line parameters.

Launching the server application with the \sh{-c} parameter followed by a file
path forces the server to use that file as its configuration file.
If this parameter is not supplied, the default behaviour is
to attempt to read \class{config.yaml} in the directory where the server
application is found. Failure to read the file will result in the server
loading the hard-coded defaults.

\subsection{Configuration options}

As mentioned the options will be read from a file. It is preferable for the
language of the file to be easy to read and write by end users.
At first JSON was considered as an option, but we felt that it was too
verbose to be used as a language for a configuration file.
When we looked at YAML, we found that their description
"YAML is a human friendly data serialization standard"\cite{yaml}
fits the above requirements nicely.
\prettyref{lst:exampleconfig} shows an example of a configuration file written
in YAML.

\subsubsection{Options}

All options and their values are case insensitive, except for keys and path
as case sensitivity is of importance for these options.
This is to ensure that options are still loaded as expected,
even if the case is not an exact match.

\begin{table}[H]
    \begin{adjustbox}{width=\textwidth}
    \begin{tabular}{llllp{5cm}}
        \toprule
        \textbf{Property}                                 & \textbf{Parameter}  & \textbf{Default value}      & \textbf{Options}                                  & \textbf{Description}                                      \\
        \midrule
        N/A                                      & \sh{-c} & \class{./config.yaml} & Any path                                 & Path to a config file                              \\
        \class{protocol}                         & N/A        & \class{TLS}        & \class{TLS}, \class{TCP}                 & Communication protocol                           \\
        \class{address}                          & \sh{-a} & \class{localhost}  & Any hostname or IP                       & Address to bind to                               \\
        \class{port}                             & \sh{-p} & \class{7000}       & Any port                                 & Port to bind to                                  \\
        \class{server\_key}                      & \sh{-k} & \class{null}       & Any string                               & Key used for server management                   \\
        \class{marshaller}                       & N/A        & \class{json}       & \class{json}                             & Marshaller used for messages                     \\
            \class{verbosity}                    & \sh{-v} & \class{0}          & \class{0} (silent), \class{1}, \class{2} & Verbosity level. High values gives more output \\
            \class{compression}                  & N/A        & \class{None}       & \class{None}                             & Compression used for messages (future use)       \\
        \class{encryption}                       & N/A        & \class{None}       & N/A                              & \\
        $\to$\class{algorithm} & N/A        & \class{None}       & \class{None}, \class{AES}                & Encryption algorithm used for all communication  \\
        $\to$\class{key}       & N/A        & \class{None}       & Some encryption key                      & Encryption key used for all communication        \\
        \bottomrule
    \end{tabular}
    \end{adjustbox}
    \caption{Properties and their corresponding command line parameter flags and
    their default values. "$\to$" indicates a nested field. N/A indicates that
the field is not applicable. The \emph{property} column are the keys used in
the configuration file, the \emph{parameter} column lists the available command
line parameters and the \emph{options} column shows what options are available
to the users.}
\end{table}

Some of the properties have limited or no options, these are intended to be
extended in the future.
Configuration options can be extended by modifying the configuration loader.

All properties are optional in the configuration file, as the application will
fall back to hard-coded defaults.
In \prettyref{lst:exampleconfig} we set all options except for
\class{compression} which does nothing for the time being.

\begin{lstlisting}[caption={Example configuration for the server application.},
    label=lst:exampleconfig]
# file name: config.yaml

protocol: tls
address: 127.0.0.1
port: 7001
server_key: SomeExampleServerKey
marshaller: json
verbosity: 2
encryption:
    algorithm: aes
    key: SomeEncryptionKey
\end{lstlisting}

\subsubsection{Java library for parsing YAML}
A YAML parser is not part of the standard Java library but third party options
exist.
We looked at the parsers listed on the official YAML project page\cite{yaml},
and found \emph{JYaml}\cite{jyaml} to be the most interesting candidate.
The project was branded as the original YAML
implementation and it suits our needs while being the easiest to use.
Version 1.3 of the library was unsuccesful in parsing the
config file, but version 1.2 succeeded. Using JYaml has its drawbacks. Version
1.2 was released back in 2007 and it is unlikely that 1.3 will ever be
released since the author of JYaml has stated that he is no longer
maintaining the project.


\emph{Jackson}\cite{jackson} was our main consideration for an alternative to JYaml.
It is a larger library with a core component that can parse JSON. The Jackson
project contains additional backends for other languages, including YAML.
\cite{jacksonyaml}
It caught our attention because its ability to parse both JSON and YAML, so
that it could also serve as a
replacement for GSON, and thus reduce the number of dependencies of the
jSpace project.
